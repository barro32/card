#!/usr/bin/env node

const boxen = require('boxen')
const chalk = require('chalk')

const boxOpts = {
    padding: 1,
    margin: 1,
    borderColor: 'green',
    borderStyle: 'round'
}

const who = chalk.green.bold('Daniel Barrington')
const what = chalk.green('Full Stack Dev')
const where = chalk.green('https://gitlab.com/barro32')

const text = `
${who}
${what}

${where}
`

console.log(boxen(text, boxOpts))
